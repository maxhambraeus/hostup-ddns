# Hostup DDNS
A simple python script to call the [hostup](https://hostup.se/) API to update the IP of a DNS record to the current IP, as detected by [icanhazip](https://icanhazip.com).

### Usage
Main file expects a `records.py` file looking something like this:
```python
RECORDS = [
    {
        'name': 'test',
        'id': '7492836',
        'type': 'A',
        'service_id': '1927',
        'zone_id': '9582'
    }

]
```

The parameters service_id, zone_id and record_id can be found with a bunch of GET requests to some endpoints (listed in their API docs, though make sure to go to the bottom where it says DNS, *NOT* where it says Domains). 

An easier way of finding it is to simply go to where you normally edit DNS records (in the web interface), click edit on the record you want to use, and look at the url. It will look like this:

```https://min.hostup.se/clientarea/services/dns/{SERVICE ID}/&act=edit_record&domain_id={ZONE ID}&record={RECORD ID}```

Then just plug the numbers into the script.

The easiest way to call it would probably be a `cron` job. 
