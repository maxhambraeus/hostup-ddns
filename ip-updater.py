import os
from dotenv import load_dotenv
import requests
from os.path import exists
from records import RECORDS

load_dotenv()

def save_refresh_token(refresh):
    try: 
        os.remove(".refresh")
    except:
        pass
    with open(".refresh", "w") as file:
        file.write(refresh)


def auth():
    if exists(".refresh"):
        payload = {}
        with open(".refresh", "r") as file:
            payload = {
                'refresh_token': f"{file.read()}"
            }

            
        req = requests.post('https://min.hostup.se/api/token', json=payload)
        token = req.json()["token"]

        refresh = req.json()["refresh"]

        save_refresh_token(refresh)
        return token


    else:
        print("You need to create a token.")
        print("Doing that for you...")
        authpayload = {
            'username': os.getenv('USERNAME'),
            'password': os.getenv('PASSWORD')
        }
        authreq = requests.post('https://min.hostup.se/api/login', json=authpayload)
        token = authreq.json()["token"]

        refresh = authreq.json()["refresh"]

        save_refresh_token(refresh)
        return token





def update_record(record, content):
    name = record['name']
    record_type = record['type']
    record_id = record['id']
    service_id = record['service_id']
    zone_id = record['zone_id']

    token = auth()
    headers = {
        'Authorization': f"Bearer {token}"
    }
    payload = {
        'name': f"{name}",
        'content': f"{content}",
        'type': f"{record_type}"
    }

    req = requests.put(f'https://min.hostup.se/api/service/{service_id}/dns/{zone_id}/records/{record_id}', headers=headers, json=payload)
    print(req.json())


def get_current_ip():
    return requests.get("https://ipv4.icanhazip.com").text


for record in RECORDS:
    update_record(record, get_current_ip())

